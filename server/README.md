# Food-Find Server

This project was cloned from [TypeScript-Node-Starter](https://github.com/Microsoft/TypeScript-Node-Starter) make sure to read its documentation in order to understand project set-up options.

# Pre-reqs

To build and run this app locally you will need a few things:

-   Install [Node.js](https://nodejs.org/en/)
-   Have a [MongoDB](https://docs.mongodb.com/manual/installation/)
    instance ready.

# Getting started

-   Clone the repository

```
git clone https://gitlab.com/NadMag/food-find.git <project_name>
```

-   Install dependencies

```
cd <project_name>
yarn
```

-   Create a .env file in the root directory accroding to the template in .env.example

-   Start your mongoDB server.

-   Build and run the project

```
yarn run build
yarn start
```

For development you can run the app in watch mode and allow debugging using:

```
yarn debug
```

For more information please refer to [TypeScript-Node-Starter](https://github.com/Microsoft/TypeScript-Node-Starter).

### API Documentation

This projects uses OpenApi with swagger-ui-express for api documentation. You can access it from the relative path to the api:
`/docs` for example: http://localhost:1234/api/v1/docs.

### Serving Client Files

In order to serve client files using this service copy the contents of the `client/build` directory into the `server/dist/client` directory in this project.
Note that you must build client files before copying them over.
