import errorHandler from "errorhandler";

import express, { Express } from "express";
import path from "path";
import compression = require("compression");
import bodyParser = require("body-parser");
import expressValidator = require("express-validator");
import lusca = require("lusca");
import cors = require("cors");
import configureRoutes from "./routes";
import env, { NODE_ENV } from "./config/env";

const app = express();
// TODO move to config file
export const staticFilePath = path.join(__dirname, "public");

export const createServer = () => {
    app.set("port", process.env.PORT || 3000);
    configureMiddlewares(app);
    configureRoutes(app);
    return {
        startListening
    };
};

const configureMiddlewares = (app: Express) => {
    app.use(compression());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(expressValidator());
    app.use(lusca.xframe("SAMEORIGIN"));
    app.use(lusca.xssProtection(true));
    if (env.NODE_ENV !== NODE_ENV.PRODUCTION) {
        app.use(errorHandler());
    }
};

/**
 * Start Express server.
 */
const startListening = () =>
    app.listen(app.get("port"), () => {
        console.log(
            "  App is running at http://localhost:%d in %s mode",
            app.get("port"),
            app.get("env")
        );
        console.log("  Press CTRL-C to stop\n");
    });
