import { configureEnvVars } from "./config/env";

import { configureMongoose } from "./config/mongoose";
import { createServer } from "./server";

configureEnvVars();
configureMongoose();
const server = createServer();
server.startListening();

export default server;
