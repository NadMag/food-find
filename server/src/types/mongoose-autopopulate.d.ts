declare module "mongoose-autopopulate" {
    import { Schema } from "mongoose";
    interface AutoPopulate {
        (schema: Schema): void;
    }
    const AutoPopulate: AutoPopulate;

    export = AutoPopulate;
}
