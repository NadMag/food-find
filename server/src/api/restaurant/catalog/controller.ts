import { Request, Response, NextFunction } from "express";
import catalogService from "./service";
import * as HttpStatus from "http-status-codes";

const all = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const docs = await catalogService.all();
        return res.status(HttpStatus.OK).json(docs);
    } catch (err) {
        return next(err);
    }
};

export default { all };
