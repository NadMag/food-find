import logger from "../../../util/logger";
import Restaurant from "../models/restaurant";

const all = async () => {
    logger.info("fetch restaurant catalog");

    const docs = (await Restaurant.find()
        .select(`_id name`)
        .lean()
        .exec()) as CatalogItem[];

    return docs;
};

export default { all };
