import express from "express";
import catalogController from "./controller";

const router = express.Router();
router.get("/catalog", catalogController.all);

export default router;
