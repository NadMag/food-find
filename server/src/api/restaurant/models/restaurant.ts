import mongoose, { Model } from "mongoose";
import { IDeliveryHours, deliveryHoursSchema } from "./deliveryHours";
import autoPopulate from "mongoose-autopopulate";

export interface IRestaurant {
    _id: string;
    name: string;
    menu: string[];
    deliveryHours: IDeliveryHours;
}

export type IRestaurantModel = mongoose.Document & IRestaurant;

const restaurantSchema = new mongoose.Schema({
    name: { type: String, required: true, unique: true },
    menu: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "MenuItem",
        required: true,
        autopopulate: true
    },
    deliveryHours: {
        type: deliveryHoursSchema,
        required: true
    }
});

restaurantSchema.plugin(autoPopulate);

const Restaurant: Model<IRestaurantModel> = mongoose.model<IRestaurantModel>(
    "Restaurant",
    restaurantSchema
);

export default Restaurant;
