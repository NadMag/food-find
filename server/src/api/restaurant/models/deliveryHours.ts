import mongoose, { Model, Document } from "mongoose";
import { IMinuteRange, minuteRangeSchema } from "./minuteRange";

export interface IDeliveryHours {
    sunday: IMinuteRange;
    monday: IMinuteRange;
    tuesday: IMinuteRange;
    wednesday: IMinuteRange;
    thursday: IMinuteRange;
    friday: IMinuteRange;
    saturday: IMinuteRange;
}

export type IDeliveryHoursModel = Document & IDeliveryHours;

const validateMinuteRange = ({ startMinute, endMinute }: IMinuteRange) =>
    startMinute < endMinute;

export const deliveryHoursSchema = new mongoose.Schema({
    sunday: {
        type: minuteRangeSchema,
        validate: validateMinuteRange
    },
    monday: {
        type: minuteRangeSchema,
        validate: validateMinuteRange
    },
    tuesday: {
        type: minuteRangeSchema,
        validate: validateMinuteRange
    },
    wednesday: {
        type: minuteRangeSchema,
        validate: validateMinuteRange
    },
    thursday: {
        type: minuteRangeSchema,
        validate: validateMinuteRange
    },
    friday: {
        type: minuteRangeSchema,
        validate: validateMinuteRange
    },
    saturday: {
        type: minuteRangeSchema,
        validate: validateMinuteRange
    }
});
