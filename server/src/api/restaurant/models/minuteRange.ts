import mongoose, { Model, Document } from "mongoose";
export interface IMinuteRange {
    startMinute: number;
    endMinute: number;
}

export type IMinuteRangeModel = Document & IMinuteRange;

export const minuteRangeSchema = new mongoose.Schema({
    startMinute: { type: Number, min: 0, max: 1440, required: true },
    endMinute: { type: Number, min: 0, max: 1440, required: true }
});

const MinuteRange: Model<IMinuteRangeModel> = mongoose.model<IMinuteRangeModel>(
    "MinuteRange",
    minuteRangeSchema
);

export default MinuteRange;
