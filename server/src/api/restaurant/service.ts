import { Types as mongooseTypes } from "mongoose";
import Restaurant, { IRestaurantModel } from "./models/restaurant";
import logger from "../../util/logger";

const all = async () => {
    logger.info("fetch all Restaurants");

    const restaurants = Restaurant.find().populate(`menu`);

    return restaurants;
};

const byId = async (id: string) => {
    logger.info(`fetch Restaurant with id ${id}`);

    const doc = await Restaurant.findById(id).populate("menu");

    return doc;
};

const create = async (restaurantData: IRestaurantModel) => {
    logger.info(`create Restaurant with data ${restaurantData}`);

    const newRest = new Restaurant(restaurantData);

    const doc = await newRest.save();

    return doc;
};

const patch = async (id: string, restaurantData: IRestaurantModel) => {
    logger.info(`update Restaurant with id ${id} with data ${restaurantData}`);

    const doc = await Restaurant.findOneAndUpdate(
        { _id: id },
        { $set: restaurantData },
        { new: true }
    );

    return doc;
};

const remove = async (id: string): Promise<void> => {
    logger.info(`delete Restaurant with id ${id}`);

    await Restaurant.findOneAndRemove({ _id: id })
        .lean()
        .exec();
};

const isValidId = (id: string) => {
    return mongooseTypes.ObjectId.isValid(id);
};

export default {
    all,
    patch,
    create,
    remove,
    byId,
    isValidId
};
