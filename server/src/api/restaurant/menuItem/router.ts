import express from "express";
import * as menuItemController from "./controller";

const rootPath = "/menu";

const router = express.Router({ mergeParams: true });

router
    .route(rootPath)
    .get(menuItemController.all)
    .post(menuItemController.create);

router
    .route(`${rootPath}/:itemId`)
    .get(menuItemController.byId)
    .patch(menuItemController.patch)
    .delete(menuItemController.remove);

export default router;
