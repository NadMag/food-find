import { Types as mongooseTypes } from "mongoose";
import MenuItem, { IMenuItemModel } from "./models/menuItem";
import logger from "../../../util/logger";
import Restaurant from "../../restaurant/models/restaurant";

export const all = async (restaurantId: string) => {
    logger.info(`Get restaurant: ${restaurantId} menu items`);

    const restaurant = await Restaurant.findById(restaurantId).populate("menu");
    return restaurant.menu;
};

export const byId = async (id: string) => {
    logger.info(`fetch MenuItem with id ${id}`);

    return await MenuItem.findById(id);
};

export const createMenuItem = async (
    restaurantId: string,
    menuItem: IMenuItemModel
) => {
    logger.info(
        `create MenuItem with data ${menuItem} in restaurant: ${restaurantId}`
    );

    const newMenuItem = new MenuItem(menuItem);
    await newMenuItem.save();
    const restaurant = await Restaurant.findById(restaurantId);
    restaurant.menu.push(newMenuItem._id);
    await restaurant.save();

    return newMenuItem;
};

export const patch = async (id: string, itemData: IMenuItemModel) => {
    logger.info(`update MenuItem with id ${id} with data ${itemData}`);

    return await MenuItem.findOneAndUpdate(
        { _id: id },
        { $set: itemData },
        { new: true }
    );
};

export const remove = async (
    restaurantId: string,
    itemId: string
): Promise<void> => {
    logger.info(`delete MenuItem with id ${itemId}`);

    Restaurant.findByIdAndUpdate(restaurantId, {
        $pull: { menu: itemId }
    }).exec();
    MenuItem.findByIdAndDelete(itemId).exec();
};

export const isValidId = (id: string) => {
    return mongooseTypes.ObjectId.isValid(id);
};
