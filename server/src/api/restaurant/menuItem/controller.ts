import { Request, Response, NextFunction } from "express";
import * as HttpStatus from "http-status-codes";
import * as menuItemService from "./service";
import { HttpError } from "../../../util/errors";

export const all = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { restaurantId } = req.params;
        const docs = await menuItemService.all(restaurantId);
        return res.status(HttpStatus.OK).json(docs);
    } catch (err) {
        return next(err);
    }
};

export const byId = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { itemId } = req.params;

        const doc = await menuItemService.byId(itemId);

        if (!doc) throw new HttpError(HttpStatus.NOT_FOUND);

        return res.status(HttpStatus.OK).json(doc);
    } catch (err) {
        return next(err);
    }
};

export const create = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const { restaurantId } = req.params;
        const doc = await menuItemService.createMenuItem(
            restaurantId,
            req.body
        );
        return res.status(HttpStatus.CREATED).json(doc);
    } catch (err) {
        return next(err);
    }
};

export const patch = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const { itemId } = req.params;
        const doc = await menuItemService.patch(itemId, req.body);
        return res.status(HttpStatus.OK).json(doc);
    } catch (err) {
        return next(err);
    }
};

export const remove = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const { restaurantId, itemId } = req.params;
        await menuItemService.remove(restaurantId, itemId);
        return res.status(HttpStatus.NO_CONTENT).send();
    } catch (err) {
        return next(err);
    }
};
