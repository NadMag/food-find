import mongoose, { Model, Document } from "mongoose";

export interface IMenuItem {
    _id: string;
    title: string;
    price: number;
}

export type IMenuItemModel = Document & IMenuItem;

export const menuItemSchema = new mongoose.Schema({
    title: { type: String, required: true },
    price: { type: Number, required: true }
});

const MenuItemModel = mongoose.model<IMenuItemModel>(
    "MenuItem",
    menuItemSchema
);

export default MenuItemModel;
