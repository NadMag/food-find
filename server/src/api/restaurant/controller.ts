import { Request, Response, NextFunction } from "express";
import * as HttpStatus from "http-status-codes";
import restaurantService from "./service";
import { HttpError } from "../../util/errors";

const all = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const docs = await restaurantService.all();
        return res.status(HttpStatus.OK).json(docs);
    } catch (err) {
        return next(err);
    }
};

const byId = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { restaurantId } = req.params;

        // Todo move to middleware or remove?
        if (!restaurantService.isValidId(restaurantId))
            throw new HttpError(HttpStatus.BAD_REQUEST);

        const doc = await restaurantService.byId(restaurantId);

        if (!doc) throw new HttpError(HttpStatus.NOT_FOUND);

        return res.status(HttpStatus.OK).json(doc);
    } catch (err) {
        return next(err);
    }
};

const create = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const doc = await restaurantService.create(req.body);
        return res.status(HttpStatus.CREATED).json(doc);
    } catch (err) {
        return next(err);
    }
};

const patch = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { restaurantId } = req.params;

        const doc = await restaurantService.patch(restaurantId, req.body);
        return res.status(HttpStatus.OK).json(doc);
    } catch (err) {
        return next(err);
    }
};

const remove = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { restaurantId } = req.params;

        const doc = await restaurantService.remove(restaurantId);
        return res.status(HttpStatus.NO_CONTENT).send();
    } catch (err) {
        return next(err);
    }
};

export default { all, byId, create, patch, remove };
