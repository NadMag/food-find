import express from "express";
import controller from "./controller";
import menuItemRouter from "./menuItem/router";

const rootPath = "/restaurants";
const restaurantPath = `${rootPath}/:restaurantId`;

const router = express.Router();

router
    .route(rootPath)
    .post(controller.create)
    .get(controller.all);

router
    .route(restaurantPath)
    .get(controller.byId)
    .patch(controller.patch)
    .delete(controller.remove);

router.use(restaurantPath, menuItemRouter);

export default router;
