import express from "express";
import swaggerUi from "swagger-ui-express";
import YAML from "yamljs";
const openApiDocument = YAML.load("src/api/apiSpec.yaml");

const router = express.Router();
router.use("/docs", swaggerUi.serve, swaggerUi.setup(openApiDocument));

export default router;
