import express, { Application } from "express";
import restaurantRouter from "./api/restaurant/router";
import catalogRouter from "./api/restaurant/catalog/router";
import openApiRouter from "./api/openApi.router";
import { staticFilePath } from "./server";
import path from "path";

const clientFilesDir = path.join(__dirname, "client");

const configureRoutes = (app: Application) => {
    app.use("/api/v1", catalogRouter, restaurantRouter, openApiRouter);
    app.use(express.static(clientFilesDir));
    app.get("/*", (req, res) => {
        res.sendFile(path.join(clientFilesDir, "index.html"));
    });
};

export default configureRoutes;
