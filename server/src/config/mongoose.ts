import mongoose from "mongoose";
import bluebird from "bluebird";
import env from "./env";
// Connect to MongoDB

export const configureMongoose = async () => {
    (<any>mongoose).Promise = bluebird;
    try {
        await mongoose.connect(env.MONGODB_URI);
    } catch (err) {
        console.log(
            "MongoDB connection error. Please make sure MongoDB is running. " +
                err
        );
    }
};
