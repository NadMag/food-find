import dotenv from "dotenv";
import fs from "fs";
import { logger } from "../util/logger";

type ProcessEnv = typeof process.env;

export enum NODE_ENV {
    PRODUCTION = "production",
    DEV = "dev"
}
export interface Environment extends ProcessEnv {
    NODE_ENV: NODE_ENV;
    WEBSERVER_URI: string;
    MONGODB_URI: string;
}

export const configureEnvVars = () => {
    if (fs.existsSync(".env")) {
        logger.debug("Using .env file to supply config environment variables");
        dotenv.config({ path: ".env" });
    }
};

const env = process.env as Environment;

export default env;
