export interface Restaurant {
    _id: string;
    name: string;
    menu: Menu;
    deliveryHours: DeliveryHours;
}

export type Menu = MenuItem[];

export interface MenuItem {
    _id: string;
    title: string;
    price: number;
}

export interface MinuteRange {
    startMinute: number;
    endMinute: number;
}

export interface CatalogItem {
    _id: string;
    name: string;
}

export type Catalog = CatalogItem[];

export interface DeliveryHours {
    sunday: MinuteRange;
    monday: MinuteRange;
    tuesday: MinuteRange;
    wednesday: MinuteRange;
    thursday: MinuteRange;
    friday: MinuteRange;
    saturday: MinuteRange;
}
