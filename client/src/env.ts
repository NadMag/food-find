interface Environment {
    [key: string]: string | undefined;
    REACT_APP_API_BASE_URI: string;
}

const ENV_VARS: Environment = {
    REACT_APP_API_BASE_URI:
        process.env.REACT_APP_API_BASE_URI || "http://localhost:3000/api/v1"
};

console.log(
    ENV_VARS.REACT_APP_API_BASE_URI,
    process.env.REACT_APP_API_BASE_URI
);

export default ENV_VARS as Environment;
