export const decorateObject = (
    original: object,
    decorator: (...args: any) => any
) =>
    Object.keys(original).reduce((decorated, key) => {
        return { ...decorated, [key]: decorator(original[key]) };
    }, {});

export const delay = async (ms: number) => {
    return new Promise(resolve => setTimeout(resolve, ms));
};

export const delayFunction = (ms: number) => (
    fn: (...args: any[]) => any | void
) => async (...args: any[]) => {
    await delay(ms);
    return fn(...args);
};

type ObjectMap<T> = { [key: string]: T };

export const groupBy = <TSource, TElement, TGroup>(
    array: TSource[],
    keySelector: (item: TSource) => string,
    elementSelector: (original: TSource) => TElement,
    groupAggregator: (group: TGroup, curr: TElement) => TGroup
): ObjectMap<TGroup> => {
    return array.reduce((acc, curr) => {
        const currKey = keySelector(curr);
        const projected = elementSelector(curr);
        acc[currKey] = groupAggregator({ ...acc[currKey] }, projected);
        return acc;
    }, {});
};

export const toObjectMap = <TSource>(
    arr: TSource[],
    keySelector: (item: TSource) => string
): ObjectMap<TSource> => groupBy(arr, keySelector, t => t, (map, t) => t);

export const toArray = <T>(
    objMap: { [key: string]: T },
    keyOrder?: string[]
): T[] => {
    if (keyOrder) return keyOrder.map(key => objMap[key]);
    return Object.values(objMap);
};

export const pad = (
    input: string,
    desiredWidth: number,
    padSymbol: string = "0"
) => {
    input = input + "";
    return input.length >= desiredWidth
        ? input
        : new Array(desiredWidth - input.length + 1).join(padSymbol) + input;
};
