import axios from "axios";
import { MenuItem } from "../types/index";
import { Omit } from "src/util/types";
import env from "src/env";

const createApiClient = (restaurantId: string) => {
    const apiPath = `${
        env.REACT_APP_API_BASE_URI
    }/restaurants/${restaurantId}/menu`;
    const client = axios.create({ baseURL: apiPath });

    const all = async () => {
        const response = await client.get<MenuItem[]>("/");
        return response;
    };

    const byId = async (id: string) => {
        const response = await client.get<MenuItem>(`/${id}`);
        return response.data;
    };

    const remove = async (id: string) => {
        return client.delete(`/${id}`);
    };

    //TODO: excluedingIds

    const create = async (item: Omit<MenuItem, "_id">) => {
        const response = await client.post<MenuItem>("/", item);
        return response.data;
    };

    const update = async (item: MenuItem) => {
        const response = await client.patch<MenuItem>(`/${item._id}`, item);
        return response.data;
    };

    return {
        all,
        byId,
        create,
        remove,
        update
    };
};

export default createApiClient;
