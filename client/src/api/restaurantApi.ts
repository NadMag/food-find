import axios from "axios";
import { Restaurant } from "src/types";
import env from "src/env";

const apiPath = `${env.REACT_APP_API_BASE_URI}/restaurants`;
const client = axios.create({ baseURL: apiPath });

export const all = async () => {
    const response = await client.get<Restaurant>("/");
    response.data;
};

export const byId = async (id: string) => {
    const response = await client.get<Restaurant>(`/${id}`);
    return response.data;
};
