import restaurantApi from "./restaurantApi";
import { Restaurant } from "src/types";

const catalogApi = {
    fetch: async () => {
        const restaurants = await restaurantApi.fetchAll();
        return restaurants.map(({ name, _id }: Restaurant) => ({ name, _id }));
    }
};

export default catalogApi;
