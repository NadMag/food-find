import { v4 } from "node-uuid";
import { decorateObject, delayFunction } from "src/util/methods";
import { CrudApi } from "./crudApi";

const createCrudApiMock = <T extends { _id: string }>(initialState: T[]) => {
    const dbMock = initialState;

    const fetchAll = async () => [...dbMock];

    // TODO find a way to fix type to not iclude id
    const create = async (item: T) => {
        const newT = Object.assign({}, item, { _id: v4() });
        dbMock.push(newT);
        return newT;
    };

    const deleteItem = async (id: string) => {
        dbMock.filter(item => item._id !== id);
    };

    const update = async (newItem: T) => {
        dbMock.map(item => (item._id === newItem._id ? newItem : item));
    };

    const byId = async (id: string) => {
        const matching = dbMock.filter(i => i._id === id);
        if (matching.length > 1) throw "Corrupted ids";
        if (matching.length == 0) throw "No such item";
        return matching[0];
    };

    const api: CrudApi<T> = {
        create,
        delete: deleteItem,
        fetchAll,
        byId,
        update
    };

    // Decorator hack to be replaced.
    const withDelay = decorateObject(api, delayFunction(500));

    return withDelay as CrudApi<T>;
};

export default createCrudApiMock;
