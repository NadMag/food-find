import createCrudApiMock from "./mockCrudApi";
import { Restaurant } from "src/types";

const restaurantApi = createCrudApiMock<Restaurant>([
    {
        _id: "5bfffbf3ebba3d71801ff666",
        name: "Soupreme",
        menu: [{ title: "Pork", price: 20, _id: "5bfff97433926b3cf43e46df" }],
        deliveryHours: {
            sunday: {
                endMinute: 820,
                startMinute: 430
            },
            monday: {
                endMinute: 820,
                startMinute: 430
            },
            tuesday: {
                endMinute: 820,
                startMinute: 430
            },
            wednesday: {
                endMinute: 820,
                startMinute: 430
            },
            thursday: {
                endMinute: 820,
                startMinute: 430
            },
            friday: {
                endMinute: 820,
                startMinute: 430
            },
            saturday: {
                endMinute: 820,
                startMinute: 430
            }
        }
    },
    {
        _id: "5bfff97433926b3cf43e46de",
        name: "p",
        deliveryHours: {
            sunday: {
                endMinute: 820,
                startMinute: 430
            },
            monday: {
                endMinute: 820,
                startMinute: 430
            },
            tuesday: {
                endMinute: 820,
                startMinute: 430
            },
            wednesday: {
                endMinute: 820,
                startMinute: 430
            },
            thursday: {
                endMinute: 820,
                startMinute: 430
            },
            friday: {
                endMinute: 820,
                startMinute: 430
            },
            saturday: {
                endMinute: 820,
                startMinute: 430
            }
        },
        menu: [{ title: "Salad", price: 20, _id: "5bfff97433926b3cf43e46df" }]
    }
]);

export default restaurantApi;
