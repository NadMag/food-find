export interface CrudApi<T extends { _id: string }> {
    fetchAll: () => Promise<T[]>;
    byId: (id: string) => Promise<T>;
    create: (item: T) => Promise<T>;
    update: (newItem: T) => Promise<void>;
    delete: (id: string) => Promise<void>;
}
