import createCrudApiMock from "./mockCrudApi";
import { MenuItem } from "src/types";

const menuApi = createCrudApiMock<MenuItem>([
    {
        title: "Pork",
        price: 20,
        _id: "5bfff97433926b3cf43e46df"
    },
    {
        title: "Salad",
        price: 20,
        _id: "5bfff97433926b3cf43e46df"
    }
]);

export default menuApi;
