import axios from "axios";
import { Catalog } from "../types/index";
import env from "src/env";

const client = axios.create({
    baseURL: `${env.REACT_APP_API_BASE_URI}/catalog`
});

export const all = async () => {
    const response = await client.get<Catalog>("/");
    return response.data;
};
