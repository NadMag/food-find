import * as React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Catalog from "./restaurant/catalog/Container";
import EditRestaurantContainer from "./restaurant/pages/EditRestaurantPageContainer";
import RestaurantContainer from "./restaurant/pages/RestaruantPageContainer";

class App extends React.Component {
    public render() {
        return (
            <Router>
                <Switch>
                    <Route
                        exact
                        path={"/restaurants/:restaurantId"}
                        render={({
                            match: {
                                params: { restaurantId }
                            }
                        }) => (
                            <RestaurantContainer restaurantId={restaurantId} />
                        )}
                    />
                    <Route
                        exact
                        path={"/manage/restaurants/:restaurantId"}
                        render={({
                            match: {
                                params: { restaurantId }
                            }
                        }) => (
                            <EditRestaurantContainer
                                restaurantId={restaurantId}
                            />
                        )}
                    />
                    <Route component={Catalog} />
                </Switch>
            </Router>
        );
    }
}

export default App;
