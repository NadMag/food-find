import * as React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

interface WithLoadingProps {
    isLoading: boolean;
}

const withLoading = <P extends object>(
    Component: React.ComponentType<P>
): React.FunctionComponent<(P | null) & WithLoadingProps> => ({
    isLoading,
    ...props
}: WithLoadingProps) =>
    isLoading || props === null ? (
        <CircularProgress />
    ) : (
        <Component {...props as P} />
    );

export default withLoading;
