import { useState, useEffect } from "react";

const useDataApi = <D>(
    fetchDataMethod: () => PromiseLike<D>,
    initialData: D,
    diffUpdateVariable?: any
) => {
    const [data, setData] = useState(initialData);
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);

    const fetchData = async () => {
        setIsError(false);
        setIsLoading(true);

        try {
            const result = await fetchDataMethod();

            setData(result);
        } catch (error) {
            console.error(error);
            setIsError(true);
        }

        setIsLoading(false);
    };

    useEffect(
        () => {
            fetchData();
        },
        //TODO: how to add diff variable (if needed)
        [diffUpdateVariable]
    );

    return { data, isLoading, isError };
};

export default useDataApi;
