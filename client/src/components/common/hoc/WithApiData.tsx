import React from "react";

interface IProps<D, R = D> {
    fetchData: (args: any) => R;
    initialData: D;
}

interface IWrappedProps<D> {
    data: D;
}

const withData = <D, R, P extends IWrappedProps<D> = IWrappedProps<D>>({
    fetchData,
    initialData
}: IProps<D, R>) => (
    WrappedComponent: React.ComponentType<P>
): React.ComponentType<P> => {
    class WithData extends React.Component<P> {
        state = {
            data: initialData,
            hasError: false,
            isLoading: false
        };

        async componentDidMount() {
            this.setState({ loading: true });
            try {
                const data = await fetchData;
                this.setState({
                    data,
                    loading: false,
                    hasError: false,
                    useDefault: data.length === 0
                });
            } catch (error) {
                console.log(error);
                this.setState({
                    hasError: true,
                    loading: false
                });
            }
        }

        render() {
            return <WrappedComponent {...this.state} {...this.props} />;
        }
    }

    return WithData;
};

export default withData;

// const ANumber = ({ number }: { number: number }) => <div>{number}</div>;
// const fetchData = async (number: number) => {
//     return (await number) + 1;
// };
// const goodNumber = withData<number, Promise<number>, { data: number }>({
//     fetchData,
//     initialData: 0
// })(({ data }) => <ANumber number={data} />);
