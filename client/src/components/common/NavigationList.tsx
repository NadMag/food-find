import List from "@material-ui/core/List";
import * as React from "react";
import { ListItemText } from "@material-ui/core";
import { ListItemLink } from "src/components/common/ListItemLink";
import { LocationDescriptor } from "history";

export type NavigationLinkProps = {
    to: LocationDescriptor;
    displayText: string;
};

export interface IProps {
    links: NavigationLinkProps[];
}

const NavigationList = ({ links }: IProps) => {
    return (
        <List>
            {links.map(({ to, displayText }) => (
                <ListItemLink key={to + displayText} to={to}>
                    <ListItemText primary={displayText} />
                </ListItemLink>
            ))}
        </List>
    );
};

export default NavigationList;
