import * as restaurantApi from "src/api/restaurantApi";
import useDataApi from "../common/hoc/withData";

export default function useRestaurantData(restaurantId: string) {
    const getRestaurant = async () => restaurantApi.byId(restaurantId);
    const dataState = useDataApi(getRestaurant, null, restaurantId);
    return dataState;
}
