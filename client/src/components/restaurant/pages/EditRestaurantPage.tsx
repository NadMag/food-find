import { Typography } from "@material-ui/core";
import * as React from "react";
import { Restaurant } from "src/types/index";
import EditableMenu from "src/components/restaurant/menu/editable/Menu";
import DeliveryHours from "src/components/restaurant/DeliveryHours";
import { ItemHandlers } from "src/components/restaurant/pages/EditRestaurantPageContainer";

export interface IProps extends ItemHandlers {
    restaurant: Restaurant | null;
}

const EditableRestaurantPage = (props: IProps) => {
    const { restaurant, ...handlers } = props;
    return restaurant === null ? null : (
        <div>
            <Typography variant="h4" align="center" gutterBottom={true}>
                {restaurant.name}
            </Typography>
            <EditableMenu items={restaurant.menu} {...handlers} />
            <DeliveryHours deliveryHours={restaurant.deliveryHours} />
        </div>
    );
};

export default EditableRestaurantPage;
