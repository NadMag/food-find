import withLoading from "src/components/common/hoc/WithLoading";
import React, { useState } from "react";
import EditRestaurantPage from "./EditRestaurantPage";
import useRestaurantData from "../useRestaurantData";
import { MenuItem } from "../../../types/index";
import menuApiClient from "src/api/menuApi";
import { Omit } from "src/util/types";

const PageWithLoading = withLoading(EditRestaurantPage);

interface IProps {
    restaurantId: string;
}

export type onItemUpdate = (newItem: MenuItem) => void;

export type onItemDelete = (id: string) => void;

export type onNewItem = (item: Omit<MenuItem, "_id">) => void;

export interface ItemHandlers {
    onItemUpdate: onItemUpdate;
    onItemDelete: onItemDelete;
    onNewItem: onNewItem;
}

const RestaurantContainer = ({ restaurantId }: IProps) => {
    const { data: initialData, isError, isLoading } = useRestaurantData(
        restaurantId
    );
    //TODO: Consider this alternative to default value rendering
    if (initialData === null) return null;

    const [restaurantState, setRestaurantState] = useState(initialData);

    const menuApi = menuApiClient(restaurantId);

    const handleMenuItemUpdate = async (item: MenuItem) => {
        //TODO: Error handling at this level?
        await menuApi.update(item);
        const afterUpdate = initialData.menu.map(i =>
            i._id === item._id ? item : i
        );
        setRestaurantState({ ...initialData, menu: afterUpdate });
    };

    const handleMenuItemDelete = async (id: string) => {
        await menuApi.remove(id);
        const afterDelete = initialData.menu.filter(i => i._id !== id);
        setRestaurantState({
            ...initialData,
            menu: afterDelete
        });
    };

    const handleNewMenuItem = async (newItem: Omit<MenuItem, "_id">) => {
        const fromServer = await menuApiClient(initialData._id).create(newItem);
        setRestaurantState({
            ...initialData,
            menu: [...initialData.menu, fromServer]
        });
    };

    return isError ? (
        <p>An Error has occurred!</p>
    ) : (
        <PageWithLoading
            isLoading={isLoading}
            restaurant={restaurantState}
            onNewItem={handleNewMenuItem}
            onItemDelete={handleMenuItemDelete}
            onItemUpdate={handleMenuItemUpdate}
        />
    );
};

export default RestaurantContainer;
