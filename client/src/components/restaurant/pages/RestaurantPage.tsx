import { Typography } from "@material-ui/core";
import * as React from "react";
import { Restaurant } from "src/types/index";
import DeliveryHours from "src/components/restaurant/DeliveryHours";
import Menu from "../../restaurant/menu/Menu";

export interface IProps {
    restaurant: Restaurant | null;
}

const RestaurantPage = ({ restaurant }: IProps) => {
    if (restaurant == null) return null;

    const { menu, deliveryHours, name } = restaurant;
    return (
        <div>
            <Typography variant="h4" align="center" gutterBottom={true}>
                {name}
            </Typography>
            <Menu items={menu} />
            <DeliveryHours deliveryHours={deliveryHours} />
        </div>
    );
};

export default RestaurantPage;
