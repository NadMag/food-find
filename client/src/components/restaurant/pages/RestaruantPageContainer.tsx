import withLoading from "src/components/common/hoc/WithLoading";
import RestaurantPage from "./RestaurantPage";
import React from "react";
import useRestaurantData from "../useRestaurantData";

const WithLoading = withLoading(RestaurantPage);

interface IProps {
    restaurantId: string;
}

const RestaurantContainer = ({ restaurantId }: IProps) => {
    const { data: restaurant, isError, isLoading } = useRestaurantData(
        restaurantId
    );
    return isError ? (
        <p>An Error has occurred!</p>
    ) : (
        <WithLoading isLoading={isLoading} restaurant={restaurant} />
    );
};

export default RestaurantContainer;
