import * as React from "react";
import Typography from "@material-ui/core/Typography";

const MenuHeader = () => {
    return (
        <Typography variant={"h6"} align={"center"}>
            Menu
        </Typography>
    );
};

export default MenuHeader;
