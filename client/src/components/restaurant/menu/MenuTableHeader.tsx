import * as React from "react";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

const MenuTableHeader = () => (
    <TableHead>
        <TableRow>
            <TableCell>Title</TableCell>
            <TableCell numeric={true}>Price</TableCell>
        </TableRow>
    </TableHead>
);

export default MenuTableHeader;
