import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Fab from "@material-ui/core/Fab";
import AddShoppingCart from "@material-ui/icons/AddShoppingCart";
import * as React from "react";
import MenuTableHeader from "./MenuTableHeader";
import MenuHeader from "./MenuHeader";
import { MenuItem } from "../../../types/index";

interface IProps {
    items: MenuItem[];
}

const Menu = ({ items }: IProps) => {
    return (
        <Paper>
            <MenuHeader />
            <Table>
                <MenuTableHeader />
                <TableBody>
                    {items.map(item => {
                        return (
                            <TableRow key={item._id}>
                                <TableCell component="th" scope="row">
                                    {item.title}
                                </TableCell>
                                <TableCell numeric={true}>
                                    {item.price}
                                </TableCell>
                                <TableCell>
                                    <Fab
                                        color="primary"
                                        aria-label="AddToCart"
                                        size="small"
                                    >
                                        <AddShoppingCart />
                                    </Fab>
                                </TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </Paper>
    );
};

export default Menu;
