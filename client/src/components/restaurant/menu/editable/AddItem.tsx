import * as React from "react";
import { MenuItem } from "src/types";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import { ChangeEvent } from "react";
import { onNewItem } from "../../pages/EditRestaurantPageContainer";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import ProductColumns from "./ProductColumns";
import { Omit } from "src/util/types";

export interface AddItemProps {
    onItemAdded: onNewItem;
}

export interface AddItemState {
    itemDraft: Omit<MenuItem, "_id">;
}

const defaultItemDisplays = {
    title: "",
    price: 0
};

export class AddItem extends React.Component<AddItemProps, AddItemState> {
    state = {
        itemDraft: defaultItemDisplays
    };

    handleItemDraftChange = (event: ChangeEvent<HTMLInputElement>) => {
        const {
            target: { name, value }
        } = event;

        this.setState(({ itemDraft }) => {
            const updatedDraft = { ...itemDraft, [name]: value };
            return { itemDraft: updatedDraft };
        });
    };

    handleAddItem = () => {
        const { itemDraft } = this.state;
        this.props.onItemAdded(itemDraft);
        this.setState({ itemDraft: defaultItemDisplays });
    };

    public render() {
        const { itemDraft } = this.state;

        return (
            <TableRow>
                <ProductColumns
                    item={itemDraft}
                    onChange={this.handleItemDraftChange}
                />
                <TableCell>
                    <Fab
                        color="primary"
                        aria-label="Add"
                        size="small"
                        onClick={this.handleAddItem}
                    >
                        <AddIcon />
                    </Fab>
                </TableCell>
            </TableRow>
        );
    }
}
