import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import React, { ChangeEvent, useState } from "react";
import Fab from "@material-ui/core/Fab";
import SaveIcon from "@material-ui/icons/Save";
import DeleteIcon from "@material-ui/icons/Delete";
import { MenuItem } from "src/types";
import {
    onItemDelete,
    onItemUpdate
} from "../../pages/EditRestaurantPageContainer";
import ProductColumns from "./ProductColumns";

interface IProps {
    onItemUpdate: onItemUpdate;
    onItemDelete: onItemDelete;
    item: MenuItem;
}

const EditableTableRow = ({ item, onItemDelete, onItemUpdate }: IProps) => {
    const [itemDraft, setDraft] = useState<MenuItem>(item);
    const [isChanged, setChanged] = useState<boolean>(false);

    const handleChange = ({
        target: { name, value }
    }: ChangeEvent<HTMLInputElement>) => {
        setDraft({ ...itemDraft, [name]: value });
        setChanged(true);
    };

    const handleSave = () => {
        onItemUpdate(itemDraft);
        setChanged(false);
    };
    const { _id } = item;
    return (
        <TableRow>
            <ProductColumns item={itemDraft} onChange={handleChange} />
            <TableCell>
                <Fab
                    disabled={!isChanged}
                    color="primary"
                    aria-label="Save"
                    size="small"
                    onClick={handleSave}
                >
                    <SaveIcon />
                </Fab>
            </TableCell>
            <TableCell>
                <Fab
                    color="primary"
                    aria-label="Delete"
                    size="small"
                    onClick={onItemDelete.bind(null, _id)}
                >
                    <DeleteIcon />
                </Fab>
            </TableCell>
        </TableRow>
    );
};

export default EditableTableRow;
