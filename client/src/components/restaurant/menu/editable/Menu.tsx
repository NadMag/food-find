import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import * as React from "react";
import MenuTableHeader from "../MenuTableHeader";
import MenuHeader from "../MenuHeader";
import { MenuItem } from "src/types";
import EditableTableRow from "./Row";
import { ItemHandlers } from "../../pages/EditRestaurantPageContainer";
import { AddItem } from "./AddItem";

interface IProps extends ItemHandlers {
    items: MenuItem[];
}

interface IState {
    items: MenuItem[];
}

export default class EditableMenu extends React.Component<IProps, IState> {
    render() {
        const { items, onNewItem, ...handlers } = this.props;

        return (
            <Paper>
                <MenuHeader />
                <Table>
                    <MenuTableHeader />
                    <TableBody>
                        {items.map(item => (
                            <EditableTableRow
                                key={item._id}
                                item={item}
                                {...handlers}
                            />
                        ))}
                        <AddItem onItemAdded={onNewItem} />
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}
