import TableCell from "@material-ui/core/TableCell";
import React, { Fragment, ChangeEvent } from "react";
import TextField from "@material-ui/core/TextField";
import { MenuItem } from "../../../../types/index";
import { Omit } from "../../../../util/types";

type OnItemChanged = (event: ChangeEvent<HTMLInputElement>) => void;

interface IProps {
    item: Omit<MenuItem, "_id">;
    onChange: OnItemChanged;
}

const ProductColumns = ({ item: { price, title }, onChange }: IProps) => {
    return (
        <Fragment>
            <TableCell component="th" scope="row">
                <TextField
                    name="title"
                    value={title}
                    margin="normal"
                    onChange={onChange}
                />
            </TableCell>
            <TableCell numeric={true}>
                <TextField
                    name="price"
                    placeholder="Enter price"
                    value={price}
                    type="number"
                    margin="normal"
                    onChange={onChange}
                />
            </TableCell>
        </Fragment>
    );
};

export default ProductColumns;
