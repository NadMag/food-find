import * as React from "react";
import CatalogPage from "src/components/restaurant/catalog/Catalog";
import { Catalog } from "../../../types/index";
import * as catalogApi from "../../../api/catalogApi";

interface IState {
    catalog: Catalog;
}

class CatalogContainer extends React.Component<{}, IState> {
    public state = { catalog: [] };

    public async componentDidMount() {
        const catalog = await catalogApi.all();
        this.setState({ catalog });
    }

    public render() {
        return <CatalogPage catalog={this.state.catalog} />;
    }
}

export default CatalogContainer;
