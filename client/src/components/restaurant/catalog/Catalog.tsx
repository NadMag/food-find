import Typography from "@material-ui/core/Typography";
import * as React from "react";
import { CatalogItem } from "../../../types/index";
import NavigationList from "../../common/NavigationList";

export interface IProps {
    catalog: CatalogItem[];
}

const RestaurantCatalog = ({ catalog }: IProps) => {
    const restaurantLinks = catalog.map(({ name, _id }) => ({
        to: `/restaurants/${_id}`,
        displayText: name
    }));

    return (
        <div className="restaurant-catalog">
            <Typography variant="display1" align="center" gutterBottom={true}>
                Restaurants
            </Typography>

            <NavigationList links={restaurantLinks} />
        </div>
    );
};

export default RestaurantCatalog;
