import * as React from "react";
import { DeliveryHours } from "../../types/index";
import Typography from "@material-ui/core/Typography";
import { pad } from "../../util/methods";
import { Paper } from "@material-ui/core";

export interface IProps {
    deliveryHours: DeliveryHours;
}

const dayNames = [
    "sunday",
    "monday",
    "tuesday",
    "wednesday",
    "thursday",
    "friday",
    "saturday"
];

// Todo organize this code into class / helper util and extract from here
const toHourString = (minutesFromMidnight: number) => {
    const hour = Math.floor(minutesFromMidnight / 60) % 24;
    const minute = minutesFromMidnight % 6;
    return `${pad(hour.toString(), 2)}:${pad(minute.toString(), 2)}`;
};

const capitalize = (input: string) =>
    input.charAt(0).toUpperCase() + input.slice(1);

const toDayHourRangeString = (
    day: string,
    startMinute: number,
    endMinute: number
) =>
    `${capitalize(day)}: ${toHourString(startMinute)}-${toHourString(
        endMinute
    )}`;

const DeliveryHours = ({ deliveryHours }: IProps) => (
    <Paper>
        <Typography variant="h6" align="center" gutterBottom={true}>
            Delivery Hours
        </Typography>
        {dayNames.map(day => {
            const { startMinute, endMinute } = deliveryHours[day];
            return (
                <Typography key={day} align="center">
                    {toDayHourRangeString(day, startMinute, endMinute)}
                </Typography>
            );
        })}
    </Paper>
);

export default DeliveryHours;
