## About

This project was bootstrapped with [Typescript React Starter](https://github.com/Microsoft/TypeScript-React-Starter). Check put its documentation in order to find out more about the project.

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Quick Start

### Run In Development

Download packages using yarn:<br>
`yarn`
<br>

Add a [.env file](#Enviroment-Setup) with values according to the format described in the .env.example file.
<br>

Compile typescript and serve using Webpack dev server.

`yarn start`
<br>

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### Routes

The default route renders a `Catalog` component which displays a list of all restaurants.
<br>

`/restaurants/<id>`<br>
The restaurant order page.<br>

`/manage/<id>/restaurant`<br>
Edit form the restaurant menu and delivery hours.

This would set up webpack-dev-server that will [proxy](#Proxying-API-Requests-in-Development) AJAX requests to localhost:1234 by default. Read more about it at the [create-react-app documentation](https://github.com/facebook/create-react-app)

## Deployment

`yarn run build` creates a `build` directory with a production build of your app. You can copy the contents of this directory into the server/dist/client directory in order to use the server for serving static files. Read more about [deploying a create-react-app](https://facebook.github.io/create-react-app/docs/deployment)

## Enviroment Setup

This project uses create-react-app [enviroment variable injection feature](https://facebook.github.io/create-react-app/docs/adding-custom-environment-variables#adding-development-environment-variables-in-env). Make sure you have the appropriate variables set up or an .env file. env file format is documented in the .dev.example files.
